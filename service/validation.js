const fileService = require('./fileService');

const reqBodyValidation = (filename, content, method) => {
  if (!filename) {
    return "Please specify 'filename' parameter";
  }

  if (!content) {
    return "Please specify 'content' parameter";
  }

  if (
    !fileService.isCorrectFileExtension(fileService.getFileExtension(filename))
  ) {
    return 'File extension is not correct';
  }

  const fileExist = fileService.isFileExist(filename);

  if (fileExist && method === 'POST') {
    return `File '${filename}' already exists`;
  }

  if (!fileExist && method === 'PUT') {
    return `No file with '${filename}' filename found `;
  }
};

module.exports = {
  reqBodyValidation,
};
