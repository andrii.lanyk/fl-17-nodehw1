const fs = require('fs');
const path = require('path');
const fsPromises = fs.promises;

const filesFolder = path.join(__dirname, '..', 'files');

const correctFileExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

try {
  if (!fs.existsSync(filesFolder)) {
    fs.mkdirSync(filesFolder);
  }
} catch (err) {
  throw err;
}

const getFilePath = (fileName) => path.join(filesFolder, fileName);

const getFileExtension = (fileName) =>
  fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length);

const isCorrectFileExtension = (extension) =>
  correctFileExtensions.includes(extension);

const isFileExist = (fileName) => fs.existsSync(getFilePath(fileName));

const getFileStatus = (fileName) => fsPromises.stat(getFilePath(fileName));

const saveFile = (fileName, content) =>
  fsPromises.writeFile(getFilePath(fileName), content);

const getFiles = () => fsPromises.readdir(filesFolder);

const getFile = (fileName) =>
  fsPromises.readFile(getFilePath(fileName), 'utf8');

const deleteFile = (fileName) => fsPromises.unlink(getFilePath(fileName));

module.exports = {
  getFileStatus,
  getFileExtension,
  saveFile,
  getFiles,
  getFile,
  deleteFile,
  isFileExist,
  isCorrectFileExtension,
};
