const express = require('express');
const logger = require('morgan');
const cors = require('cors');

const filesRoutes = require('./routes/routes');

const app = express();

const PORT = process.env.PORT || 8080;

const formatsLogger = app.get('env') === 'development' ? 'dev' : 'short';

app.use(logger(formatsLogger));

app.use(cors());
app.use(express.json());

app.use('/api/files', filesRoutes);

app.listen(PORT, () => {
  console.log(`Server has been started on port ${PORT}`);
});
